#include "class.h"



Gcode_Correction_class::Gcode_Correction_class(std::string filename, double d, double D, double lunghezza_corda)
{
    Gcode_Correction_class::filename = filename;
    Gcode_Correction_class::d = d;
    Gcode_Correction_class::D = D;
    Gcode_Correction_class::LUNGHEZZA_CORDA = lunghezza_corda;
}

Gcode_Correction_class::~Gcode_Correction_class()
{
}

std::string Gcode_Correction_class::leggiFileGcode() {
    

    std :: ifstream gcodeFile(Gcode_Correction_class::filename);
    std :: string line;
    std :: string file = "";

    if(gcodeFile.is_open()) {

        while(std::getline(gcodeFile, line)) {

            file += line + "\n";

        }

        gcodeFile.close();

    } else {
        std::cout << "Non riesco aprire il file";
    }

    return file;
}


bool Gcode_Correction_class::scriviGcode(std::string fileGcode) {
    bool eseguito;
    std::ofstream fileCorretto;
    try {
        fileCorretto.open("fileCorretto.gcode");
        fileCorretto << fileGcode;
        fileCorretto.close();
        eseguito = true;
    } catch(const std::ofstream::failure e) {
        eseguito = false;
    }

    return eseguito;
}
/*
void  Gcode_Correction_class::scriviLog(std::string messaggio) {

    

    if(std::ifstream("log.log")) {
        std::cout << "File already exists" << std::endl;

        std::ofstream logFile;

        logFile.open("log.log", std::ios_base::app);

        time_t now = time(0);
        char* datetime = ctime(&now);

        logFile << datetime << " - " << messaggio << std::endl;

        logFile.close();
        
    } else {
        std::cout << "File not exists" << std::endl;

        std::ofstream logFile("log.log");


        time_t now = time(0);
        char* datetime = ctime(&now);

        logFile << datetime << " - " << messaggio << std::endl;

        logFile.close();

    }

}*/

double Gcode_Correction_class::lunghezzaCordaDx(Coordinate punto, Coordinate delta) {
    double ris = sqrt(pow((delta.x +punto.x), 2) + pow((delta.y + punto.y), 2));
    return ris;
}

double Gcode_Correction_class::lunghezzaCordaSx(Coordinate punto, Coordinate delta) {
    double ris = sqrt(pow((D - delta.x +punto.x), 2) + pow((delta.y + punto.y), 2));
    return ris;
}

Coordinate Gcode_Correction_class::conversione_coordinate(Coordinate originali) {

    Coordinate convertite;

    double xOffset = sqrt(pow(LUNGHEZZA_CORDA,2) - pow( ( (D -d) / 2) , 2));
    double yOffset = D / 2;

    double xTrasposta = xOffset + originali.x;
    double yTrasposta = yOffset + originali.y;

    double lunghezza_corda_sx = sqrt(pow(yTrasposta,2) + pow( xTrasposta - (d/2) , 2));
    double lunghezza_corda_dx = sqrt(pow(yTrasposta,2) + pow(D - (d /2) - xTrasposta, 2));

    convertite.x = lunghezza_corda_sx - LUNGHEZZA_CORDA;
    convertite.y = lunghezza_corda_dx - LUNGHEZZA_CORDA;

    /*double altezza = sqrt(pow(LUNGHEZZA_CORDA, 2) - (D/2-d/2) * (D/2-d/2));
    convertite.x = originali.x + (D/2) - (d/2);
    convertite.y = originali.y+altezza;*/

    return convertite;
}

std::vector<Coordinate> Gcode_Correction_class::segmentazioneSegmentiLineari(Coordinate partenza, Coordinate arrivo) {
    std::string comandi = "";
    double lunghezzaSegmento = sqrt( (pow((arrivo.x-partenza.x), 2)) + (pow(arrivo.y-partenza.y, 2)));
    //std::cout << lunghezzaSegmento << std::endl;
    int nSegmenti = floor(lunghezzaSegmento / 5);
    //std::cout << nSegmenti << std::endl;
    double deltaX = (arrivo.x-partenza.x) / nSegmenti;
    double deltaY = (arrivo.y-partenza.y) / nSegmenti;

    Coordinate puntiTrovati[nSegmenti]; 

    
    for(int i = 0; i < nSegmenti; i++) {
       puntiTrovati[i].x = puntiTrovati[i - 1].x + deltaX;
       puntiTrovati[i].y = puntiTrovati[i -1].y + deltaY;
    }

    puntiTrovati[nSegmenti].x = arrivo.x;
    puntiTrovati[nSegmenti].y = arrivo.y;

    std::vector<Coordinate> vectorC(puntiTrovati, puntiTrovati + sizeof(puntiTrovati) / sizeof(Coordinate));


    return vectorC;

}

Coordinate Gcode_Correction_class::conversione_polare(Coordinate originali) {
    Coordinate polari;
    polari.y = atan2(originali.y, originali.x);
    polari.x = sqrt(pow(originali.x, 2) + pow(originali.y, 2));
    return polari;
}

Coordinate Gcode_Correction_class::conversione_cartesiane(Coordinate polari) {
    Coordinate c;
    c.x = polari.x * cos(polari.y);
    c.y = polari.x * sin(polari.y);
    return c;
}

Coordinate Gcode_Correction_class::muoviPunto(Coordinate partenza, double x_centro, double y_centro) {
    partenza.x += x_centro;
    partenza.y += y_centro;

    return partenza;
}

std::vector<Coordinate> Gcode_Correction_class::segmentazioneArchiDiCerchio(Coordinate puntoPartenza, Coordinate puntoCentro, Coordinate puntoArrivo, bool orario) {
    std::vector<Coordinate> punti;
    Coordinate partenza_polare = Gcode_Correction_class::conversione_polare(Gcode_Correction_class::muoviPunto(puntoPartenza, -puntoCentro.x, -puntoCentro.y));
    Coordinate arrivo_polare = Gcode_Correction_class::conversione_polare(Gcode_Correction_class::muoviPunto(puntoArrivo, -puntoCentro.x, -puntoCentro.y));

    double ampiezzaAngoloSuddivisione = 0.1; //radianti

    double distanza = sqrt(pow((puntoPartenza.x - puntoArrivo.x),2) + pow((puntoPartenza.y - puntoArrivo.y),2));

    if(distanza > 50 ) {
        if(orario) {
            if(arrivo_polare.y < partenza_polare.y) {
                arrivo_polare.y += 2.0 * PI;
            }

            partenza_polare.y += ampiezzaAngoloSuddivisione;

            while(partenza_polare.y < arrivo_polare.y) {
                Coordinate p = Gcode_Correction_class::conversione_cartesiane(partenza_polare);
                p = Gcode_Correction_class::muoviPunto(p, puntoCentro.x, puntoCentro.y);
                punti.push_back(p);
                partenza_polare.y += ampiezzaAngoloSuddivisione;
            }
        } else {
            if(arrivo_polare.y > partenza_polare.y) {
                arrivo_polare.y -= 2.0 * PI;
            }

            partenza_polare.y -= ampiezzaAngoloSuddivisione;

            while(partenza_polare.y > arrivo_polare.y) {
                Coordinate p = Gcode_Correction_class::conversione_cartesiane(partenza_polare);
                p = Gcode_Correction_class::muoviPunto(p, puntoCentro.x, puntoCentro.y);
                punti.push_back(p);
                partenza_polare.y -= ampiezzaAngoloSuddivisione;
            }
        }
    }

    
    //std::cout << "Ultimo -> " << puntoArrivo.x << " " << puntoArrivo.y << "\n";
    punti.push_back(puntoArrivo);

    return punti;
}


std::string* Gcode_Correction_class::split(std::string* array, std::string fileGcode) {
    std::string delimitatore = "\n";

    size_t posizione = 0;

    std::string linea;

    int i = 0;

    while((posizione = fileGcode.find(delimitatore)) != std::string::npos) {

        linea = fileGcode.substr(0, posizione);



        array[i] = linea;



        fileGcode.erase(0, posizione + delimitatore.length());

        i++;

    }

    return array;
}



std::string Gcode_Correction_class::modificaGcode() {
    std::string fileGcode = leggiFileGcode();
    size_t nLineeGcode = std::count(fileGcode.begin(), fileGcode.end(), '\n');

    //std::cout << nLineeGcode << std::endl << fileGcode << std::endl;
    //Array per contenere le linee in un array


    //Tolgo gli spazi multipli
    std::string::iterator new_end = std::unique(fileGcode.begin(), fileGcode.end(), BothAreSpaces);
    fileGcode.erase(new_end, fileGcode.end());

    //Split
    std::string lineeFile[nLineeGcode];
    std::string *punt_lineeFile = Gcode_Correction_class::split(lineeFile, fileGcode);

    //Variabili per la modifica
    std::string linea;
    std::string stringa_splittata;
    Comando comando;
    std::vector<std::string> array_stringhe_splittate;
    std::string file_corretto = "";
    bool esiste_precedente = false;
    Coordinate precedente;

    for(int i = 0; i < nLineeGcode; i++) {
        //std::cout << lineeFile[i] << std::endl;
        linea = lineeFile[i];
        std::istringstream iss(linea);
        //? Trovare REGEX PER PRENDERE SOLTANTO G1,G2,G3
        std::string regex = "\\b(G1|G2|G3)\\b";
        std::regex trova_comando (regex);
        std::smatch match;
        if(linea.find('X') != std::string::npos && linea.find('Y') != std::string::npos) {
            while(std::getline(iss, stringa_splittata, ' ')) {
                array_stringhe_splittate.push_back(stringa_splittata);
            }

            comando.comando = array_stringhe_splittate[0];
            std::string x = array_stringhe_splittate[1].substr(1);
            std::string y = array_stringhe_splittate[2].substr(1);
            //Regex giusta -> ^(.*?(\bG1|G2|G3\b)[^$]*)$
            if(std::regex_search(linea,match, trova_comando)) {
                //::cout << "Match" << std::endl;
                //std::cout << linea << std::endl;

                
                    if(comando.comando == "G1") {

                        //std::cout << "G1" << std::endl;
                        

                        comando.arrivo.x = atof(x.c_str());
                        comando.arrivo.y = atof(y.c_str());
                        //comando.arrivo = conversione_coordinate(comando.arrivo);

                        if(esiste_precedente) {
                            //std::cout << "segmentazioneSegmentiLineari con prec\n";
                            //file_corretto += segmentazioneSegmentiLineari(precedente, comando.arrivo);
                            std::vector<Coordinate> v = segmentazioneSegmentiLineari(precedente, comando.arrivo);
                            for(Coordinate c : v) {
                                c = conversione_coordinate(c);
                                std::stringstream ss;
                                ss << c.x;
                                file_corretto += "G1 X" + ss.str() + " Y" ;
                                ss.str("");
                                ss << c.y;
                                file_corretto += ss.str() + "\n";
                            }
                        } else {
                            Coordinate c;
                            c.x = atof(x.c_str());
                            c.y = atof(y.c_str());
                            c = conversione_coordinate(c);
                            std::stringstream ss;
                            ss << c.x;
                            file_corretto += "G1 X" + ss.str() + " Y" ;
                            ss.str("");
                            ss << c.y;
                            file_corretto += ss.str() + "\n";
                        }

                        precedente.x = comando.arrivo.x;
                        precedente.y = comando.arrivo.y;


                    } else if(comando.comando == "G2" || comando.comando == "G3") {
                        //std::cout << "G2/G3" << std::endl;
                        //forse controllo se c'e' I e J
                        std::string xCentro = array_stringhe_splittate[3].substr(1);
                        std::string yCentro = array_stringhe_splittate[4].substr(1);
                        //std::cout << "Centro-> " << xCentro << " " << yCentro << std::endl;
                        comando.arrivo.x = atof(x.c_str());
                        comando.arrivo.y = atof(y.c_str());
                        comando.centro.x = atof(xCentro.c_str());
                        comando.centro.y = atof(yCentro.c_str());
                        //comando.arrivo = conversione_coordinate(comando.arrivo);
                        //comando.centro = conversione_coordinate(comando.centro);

                        if(esiste_precedente) {
                            //std::cout << "segmentazioneArchi con prec\n";
                            bool orario = comando.comando == "G2" ? true : false;
                            std::vector<Coordinate> v = segmentazioneArchiDiCerchio(precedente, comando.centro ,comando.arrivo, orario);
                            //std::cout << "Ultimo -> " << v.at(v.size() - 1).x << " " << v.at(v.size() - 1).y << "\n";
                            for(Coordinate c : v) {
                                c = conversione_coordinate(c);
                                std::stringstream ss;
                                ss << c.x;
                                file_corretto += "G1 X" + ss.str() + " Y" ;
                                ss.str("");
                                ss << c.y;
                                file_corretto += ss.str() + "\n";
                            }
                            precedente.x = v.at(v.size() - 1).x;
                            precedente.y = v.at(v.size() - 1).y;

                        }

                    } 

                    //precedente.x = atof(x.c_str());
                    //precedente.y = atof(y.c_str());
                    //std::cout << precedente.x << " " << precedente.y << std::endl;
                    //precedente = conversione_coordinate(precedente);

                    esiste_precedente = true;

                    array_stringhe_splittate.clear();

                } else {
                        
                    size_t posizioneX = linea.find('X');
                    size_t delimitatore = linea.find(' ', posizioneX);
                    comando.arrivo.x = atof(linea.substr(posizioneX+1, delimitatore - posizioneX).c_str());
                    size_t posizioneY = linea.find('Y');
                    delimitatore = linea.find(' ', posizioneY);
                    comando.arrivo.y = atof(linea.substr(posizioneY+1, delimitatore - posizioneY).c_str());

                    //comando.arrivo = conversione_coordinate(comando.arrivo);
                    std::string lineaCorretta;
                    std::stringstream ss;

                    lineaCorretta = linea.substr(0, posizioneX+1);
                    ss <<comando.arrivo.x;
                    lineaCorretta += ss.str() + " Y";
                    ss.str("");
                    ss << comando.arrivo.y;
                    lineaCorretta += ss.str();
                    lineaCorretta += linea.substr(delimitatore);
                    file_corretto += lineaCorretta+"\n";


                    precedente.x = atof(x.c_str());
                    precedente.y = atof(y.c_str());

                    //precedente = conversione_coordinate(precedente);

                    esiste_precedente = true;

                    //std::cout << "No Match" << std::endl;
                    //file_corretto += linea + "\n";

                }
        } else {
            //std::cout << linea << " NO REGX\n";
            file_corretto += linea + "\n";
        }

    }

    scriviGcode(file_corretto);

    return file_corretto;
}

