#include <string>
#include <iostream>
#include <math.h>
#include <vector>

using namespace std;

const double PI = 3.141592653589793238463;

class Point {
    public:
        //Costruttore di copia
        Point(const Point& originale) {
            x = originale.x;
            y = originale.y;
        }
        //Costruttore di inizializzazione
        Point(double _x = 0.0, double _y = 0.0) {
            x = _x;
            y = _y;
        }

        double x;
        double y;
};
//Conversione a polare
Point toPolar(Point p) {
    Point polar;
    polar.y = atan2(p.y, p.x);
    polar.x = sqrt(p.x * p.x + p.y * p.y);
    return polar;
}
//Conversione a cartesiano
Point toCartesian(Point polar) {
    Point cart;
    cart.x = polar.x * cos(polar.y);
    cart.y = polar.x * sin(polar.y);
    return cart;
}
Point movePoint(Point p, double x, double y){
    p.x += x;
    p.y += y;
    return p;
    
}

//Classe per lanciare i metodi di modifica del gcode
class Geometry {
public:
    Geometry() {}
    Geometry(const Geometry& orig) {}
    virtual ~Geometry() {}
    //Serve per trovare il centro del cerchio, dato il raggio, punto di partenza e punto di arrrivo + un bool per sapere se e' orario o antiorario
    Point getArcCenterPoint(Point begin, Point end, double r, bool ccw){

    double distance;
        double dist_center;

        // distance between begin and end:
        distance = sqrt(pow((end.x - begin.x), 2.0) + pow((end.y - begin.y), 2.0));

        // center point from the line from begin to end:
        Point middle((begin.x + end.x) / 2.0, (begin.y + end.y) / 2.0);
        //middle.y() = ;

        // center point of the circle:
        double x;
        double y;
        if (ccw) {  // counter clockwise
            x = middle.x + sqrt(pow(r, 2.0) - pow((distance / 2.0), 2.0)) * (begin.y - end.y) / distance;
            y = middle.y + sqrt(pow(r, 2.0) - pow((distance / 2.0), 2.0)) * (end.x - begin.x) / distance;
        } else {
            x = middle.x - sqrt(pow(r, 2.0) - pow((distance / 2.0), 2.0)) * (begin.y - end.y) / distance;
            y = middle.y - sqrt(pow(r, 2.0) - pow((distance / 2.0), 2.0)) * (end.x - begin.x) / distance;

        }
        Point center(x, y);

        return center;
    }
    std::vector<Point> getArcPolygon(Point begin, Point end, double r, bool ccw) {
        std::cout << " begin=" << begin.x << ", " << begin.y << std::endl;
        Point center = this->getArcCenterPoint(begin, end, r, ccw);
        center.x = 5;
        center.y = 5;
        std::cout << " center=" << center.x << ", " << center.y << ", r=" << r << std::endl;
        std::cout << " end=" << end.x << ", " << end.y << std::endl;
        std::vector<Point> points;

        Point begin_polar = toPolar(movePoint(begin, -center.x, -center.y));
        std::cout << " begin trans polar=" << begin_polar.x << ", " << begin_polar.y << std::endl;    

        Point end_polar = toPolar(movePoint(end, -center.x, -center.y));
        std::cout << " end trans polar=" << end_polar.x << ", " << end_polar.y << std::endl;



        double delta_angle = 1; // rad
        //points.push_back(begin);        
        if (ccw) {
            if (end_polar.y < begin_polar.y) {
                end_polar.y += 2.0 * PI;
            }
            begin_polar.y += delta_angle;
            while (begin_polar.y < end_polar.y) {
                Point p = toCartesian(begin_polar);
                p = movePoint(p, center.x, center.y);
                points.push_back(p);
                begin_polar.y += delta_angle;
            }



        } else {
            if (end_polar.y > begin_polar.y) {
                end_polar.y -= 2.0 * PI;
            }
            begin_polar.y -= delta_angle;
            while (begin_polar.y > end_polar.y) {
                Point p = toCartesian(begin_polar);
                p = movePoint(p, center.x, center.y);
                points.push_back(p);
                begin_polar.y -= delta_angle;
            }


        }
        //std::cout << " end=" << end.x << ", " << end.y << std::endl;
        points.push_back(end);

        return points;
        }

private:

};


int main() {
    //G1 X15.4547 Y50.8589
    //G2 X46.8715 Y51.2119 I31.5266 J-1566.5253
    Point p2 (46.8715, 51.2119);
    Point p1 (15.4547,50.8589);

    Geometry g;

    std::vector<Point> points = g.getArcPolygon(p1, p2, 5.0, false);
    for (int i = 0; i < points.size(); i++) {
        std::cout << "G1 X" <<points[i].x << " Y" << points[i].y << std::endl;
    }

   return 0;
}
