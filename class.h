#include <iostream>
#include <string>
#include <iostream>
#include <cmath>
#include <math.h>
#include <fstream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <regex>
#include <sstream>


const double PI = 3.141592653589793238463;

struct Coordinate
{
    double x;
    double y;
};

struct Comando {
    std::string comando;
    Coordinate arrivo;
    Coordinate centro;
};


bool static BothAreSpaces(char lhs, char rhs) {
    return (lhs == rhs) && (lhs == ' ');
}




class Gcode_Correction_class
{
private:
    std::string filename;
    double K {0.5};
    double LUNGHEZZA_CORDA {400.0};
    double d {120.0};
    double D {720.0};




public:
    Gcode_Correction_class(std::string filename, double d, double D, double lunghezza_corda);
    ~Gcode_Correction_class();
    //File IO Methods
    std::string leggiFileGcode();
    bool scriviGcode(std::string fileGcode);
    void scriviLog(std::string messaggio);


    double lunghezzaCordaDx(Coordinate punto, Coordinate delta);
    double lunghezzaCordaSx(Coordinate punto, Coordinate delta);
    Coordinate conversione_coordinate(Coordinate originali);
    Coordinate conversione_polare(Coordinate originali);
    Coordinate conversione_cartesiane(Coordinate polari);
    Coordinate muoviPunto(Coordinate partenza, double x_centro, double y_centro);

    //Correction Gcode Method
    std::vector<Coordinate> segmentazioneSegmentiLineari(Coordinate partenza, Coordinate arrivo);
    std::vector<Coordinate> segmentazioneArchiDiCerchio(Coordinate puntoPartenza, Coordinate puntoCentro, Coordinate puntoArrivo, bool orario);
    std::string* split(std::string* array, std::string fileGcode);
    std::string modificaGcode();
};