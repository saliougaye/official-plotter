(Scribbled version of C:\Users\ricky\AppData\Local\Temp\ink_ext_XXXXXX.svg1D68K0 @ 3500.00)
( unicorn.py --tab="plotter_setup" --pen-up-angle=50 --pen-down-angle=30 --start-delay=150 --stop-delay=150 --xy-feedrate=3500 --z-feedrate=150 --z-height=0 --finished-height=0 --register-pen=true --x-home=0 --y-home=0 --num-copies=1 --continuous=false --pause-on-layer-change=false C:\Users\ricky\AppData\Local\Temp\ink_ext_XXXXXX.svg1D68K0 )
G21 (metric ftw)
G90 (absolute mode)
G92 X0.00 Y0.00 Z0.00 (you are here)

M300 S30 (pen down)
G4 P150 (wait 150ms)
M300 S50 (pen up)
G4 P150 (wait 150ms)
M18 (disengage drives)
M01 (Was registration test successful?)
M17 (engage drives if YES, and continue)

(Polyline consisting of 1 segments.)
G1 X22.00 Y-15.05 F3500.00
M300 S30.00 (pen down)
G4 P150 (wait 150ms)
G1 X-5.94 Y-0.18 F3500.00
G1 X-34.03 Y-14.76 F3500.00
G1 X-28.52 Y16.41 F3500.00
G1 X-51.07 Y38.62 F3500.00
G1 X-19.73 Y43.01 F3500.00
G1 X-5.58 Y71.32 F3500.00
G1 X8.29 Y42.86 F3500.00
G1 X39.59 Y38.15 F3500.00
G1 X16.81 Y16.17 F3500.00
G1 X22.00 Y-15.05 F3500.00
M300 S50.00 (pen up)
G4 P150 (wait 150ms)


(end of print job)
M300 S50.00 (pen up)
G4 P150 (wait 150ms)
M300 S255 (turn off servo)
G1 X0 Y0 F3500.00
G1 Z0.00 F150.00 (go up to finished level)
G1 X0.00 Y0.00 F3500.00 (go home)
M18 (drives off)
